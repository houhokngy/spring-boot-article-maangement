package info.hokngy.service;

import java.util.List;

import info.hokngy.model.Article;

public interface ArticleService {
	List<Article> findAll();
	Article findOne(int id);
	boolean remove(int id);
	boolean save(Article article);
	boolean update(Article article);
}
