package info.hokngy.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import info.hokngy.model.Article;
import info.hokngy.service.ArticleService;

@Controller
public class ArticleController extends WebMvcConfigurerAdapter {

	private ArticleService articleService;

	@Autowired
	public ArticleController(ArticleService articleService) {
		this.articleService = articleService;
	}

	@RequestMapping(value = { "/article", "/" }, method = RequestMethod.GET)
	public String homePage(ModelMap model) {
		List<Article> articles = articleService.findAll();
		model.addAttribute("articles", articles);
		model.addAttribute("lang", "lang");
		//return "article";
		return "article";
	}

	@RequestMapping(value = "/article/aid", method = RequestMethod.GET)
	public String articleDetail(ModelMap model, @RequestParam("id") Integer id) {
		Article article = articleService.findOne(id +1);
		model.addAttribute("article", article);
		return "article-detail";
	}

	@RequestMapping("/article/remove")
	public String remove(ModelMap model, @RequestParam("id") Integer id) {
		if (articleService.remove(id)) {
			System.out.println("you remove successfully!");
		}
		return "redirect:/article";
	}

	@PostMapping("/article/save")
	public String save(@Valid @ModelAttribute("article") Article article, BindingResult result) {
		if (result.hasErrors()) {
			return "add-article";
		}
		if (articleService.save(article)) {
			System.out.println("success!!!");
		}
		return "redirect:/article";
	}

	@GetMapping("/article/add")
	public String addpage(ModelMap model) {
		model.addAttribute("article", new Article());
		model.addAttribute("action", "/article/save");
		return "add-article";
	}

	@GetMapping("/article/update")
	public String edit(ModelMap model, @RequestParam("id") Integer id) {
		model.addAttribute("article", articleService.findOne(id));
		// model.addAttribute("action", "/article/update");
		model.addAttribute("addStatus", false);
		return "update-article";
	}

	@PostMapping("/article/update")
	public String update(@ModelAttribute("article") Article article, BindingResult result) {
		if (result.hasErrors()) {
			return "redirect:/article/add";
		}
		if (articleService.update(article)) {
			System.out.println("Successfully!!!");
		}
		return "redirect:/article";
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
		sessionLocaleResolver.setDefaultLocale(Locale.US);
		return sessionLocaleResolver;
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}

	@RequestMapping(value = "/article/upload", method = RequestMethod.GET)
	public String upload() {
		return "upload";
	}

	@RequestMapping(value = "/article/upload", method = RequestMethod.POST)
	public @ResponseBody String upload1(@RequestParam("file") MultipartFile file) throws IOException {
		if (!file.isEmpty()) {
			byte[] f = file.getBytes();
			Path path = Paths.get("src/main/resources/myFile" + file.getOriginalFilename());
			Files.write(path, f);
		}
		return "upload";
	
	}
	

	

	@RequestMapping(value = { "/kk", "/" })
	public String kk(ModelMap model) {
		return "article-detail2";
		
	}


	
	
}
