package info.hokngy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootArticleMaangementApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootArticleMaangementApplication.class, args);
	}
}
