package info.hokngy.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Repository;

import com.github.javafaker.Faker;

import info.hokngy.model.Article;

@Repository
public class ArticleRepositoryImp implements ArticleRepository {
	private List<Article> articles;

	public ArticleRepositoryImp() {
		Faker faker = new Faker(Locale.ENGLISH);
		articles = new ArrayList<Article>();
		for (int i = 0; i < 30; i++) {
			Article article = new Article();
			article.setId(i + 1);
			article.setTitle(faker.book().title());
			article.setDescription(faker.lorem().sentence());
			article.setThumbnail(faker.internet().image(100, 60, false, ""));
			articles.add(article);
		}
	}

	@Override
	public List<Article> findAll() {
		return articles;
	}

	@Override
	public Article findOne(int id) {
		return articles.get(id - 1);
	}

	@Override
	public boolean save(Article article) {
		return articles.add(article);
	}

	@Override
	public boolean remove(int id) {
		for (Article article : articles) {
			if (article.getId() == id) {
				articles.remove(article);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean update(Article article) {
		for (int i = 0; i <= articles.size(); i++) {
			if (articles.get(i).getId() == article.getId()) {
				articles.get(i).setId(article.getId());
				articles.get(i).setTitle(article.getTitle());
				articles.get(i).setDescription(article.getDescription());
				articles.get(i).setThumbnail(article.getThumbnail());
				return true;
			}
		}
		return false;
	}
}
